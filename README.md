moons1D
==============================

1D simulator for MOONS/VLT

####Install Moons1d

```
git clone https://gitlab.obspm.fr/mrodrigues/moons1d.git
cd moons1d/
sudo python setup.py install
```


####Run scripts

```
cd scripts
python MultipleSource.py
```


####Run notebooks
Use your favorite jupyter notebook server, e.g. https://nteract.io, to open the notebooks (with a python3 kernel

####Documentation
https://moons1d.readthedocs.io/en/latest/index.html#
