.. _MultipleTargets:

***********************************
Run simulation for multiple targets  
***********************************

Generate Input catalogue and templates
======================================

.. code-block:: python

   # Generate a catalogue from uniform distribution of magnitudes and redshift 
   n_target = 10
   mag_range = [16.,21.]
   mag_band = 'H'
   band_center = 8000 #in the same units as the template. here angstrom
   band_with = 1000
   redshift_range = [0.,0.]
   catalogue = Catalogue_Unidistribution(Obs_conf['Obs_ID'], n_target, mag_range , mag_band, redshift_range)
   
.. code-block:: python
   
   for target in catalogue :
       target.setTemplate(Path_template+File_Template)
       target.setOutFile(Path_output+target.name+'.fits')   
   Save_Catalogue(catalogue,Path_output+Obs_conf['Obs_ID']+'/Catalogue_'+Obs_conf['Obs_ID']+'.dat') 
   
   
Initialise simulation
=====================

Same as Single source simulation. 

.. code-block:: python

   Simu = Simulation(MOONS_modes,Obs_conf)
   Simu.GenerateDispersionAxis() 

   # Precompute the throughtput curves for the observing mode 
   Simu.Generate_Telescope_Transmission()
   Simu.Generate_Instrument_Transmission()
   Simu.Generate_FibreLoss(atm_disp = True) 
   Simu.Generate_Total_Transmission()

   # Generate sky emission and absorption spectra 
   sky_model = Sky(1)
   sky_model.Load_ESOSkyCal_Template(Simu.airmass) 
   atm_model = Atm_abs(1)
   atm_model.Load_ESOSkyCal_Template(Simu.airmass) 
   #Attach to simulation instance
   Simu.setSky(sky_model) 
   Simu.setAtms_abs(atm_model)
   


Run simulation for each target
==============================

.. code-block:: python

   for target in catalogue :
       redshift =  target.getRedshift()
       mag =  target.getMag()
    
       target_spectrum = Template(1, type_source ='point-source') 
       target_spectrum.Load_Templates_File(target.template)

       # Normalise to a given apparent magnitude 
       target_spectrum.SetMagnitude([band_center,band_with], target.mag)
	   # Attach the source to the simulation 
	   Simu.setTemplate(target_spectrum
	   # Output .fits file
	   Simu.setFileOut(Path_output + Simu.OB_name + '/' + Simu.OB_name + '_' + target.name +'.fits')
	   
.. code-block:: python
	   
	   Simu.Match_resolution() 
	   Simu.ConvertFlux2Counts()  
	   Simu.sky.CreateSkyMask(sigma=3.2)
	   Simu.Generate_Nod_Seq(stack='sum')
	   Units_calib = u.erg / u.s / u.cm**2/ u.angstrom
	   Simu.Calibrate_data(units = Units_calib)
	   #Save fits file
	   Simu.SavetoFits()
	   
	   