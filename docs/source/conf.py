# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import os
import sys
import warnings
from astropy.utils import minversion
from pkg_resources import get_distribution
sys.path.insert(0, os.path.abspath('../../'))
sys.setrecursionlimit(1500)

IPYTHON_LT_7_1 = not minversion('IPython', '7.1.0')

import mock
MOCK_MODULES = []
for mod_name in MOCK_MODULES:
    sys.modules[mod_name] = mock.Mock()

# -- Project information -----------------------------------------------------

project = 'Moons1D'
copyright = '2022, Myriam Rodrigues'
author = 'Myriam Rodrigues'

# The full version, including alpha/beta/rc tags
release = '1.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.extlinks',
    'sphinx.ext.ifconfig',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
    'numpydoc',
    'IPython.sphinxext.ipython_console_highlighting',
    'sphinx_automodapi.automodapi',
    'sphinx_automodapi.smart_resolver',
    'recommonmark','sphinx.ext.napoleon'
]
todo_include_todos=True
if IPYTHON_LT_7_1:
    # before IPython 7.1 we used our own custom version of the extension
    warnings.warn(
        "IPython 7.1 or later is required to build the code examples"
    )
else:
    extensions.append('IPython.sphinxext.ipython_directive')

try:
    import matplotlib.sphinxext.plot_directive
    extensions += [matplotlib.sphinxext.plot_directive.__name__]
# AttributeError is checked here in case matplotlib is installed but
# Sphinx isn't.  Note that this module is imported by the config file
# generator, even if we're not building the docs.
except (ImportError, AttributeError):
    warnings.warn(
        "matplotlib's plot_directive could not be imported. "
        "Inline plots will not be included in the output"
    )


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []
master_doc = 'index'

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'numpy': ('https://docs.scipy.org/doc/numpy/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
    'matplotlib': ('https://matplotlib.org/', None),
    'astropy': ('https://docs.astropy.org/en/stable/', None),
    'synphot': ('https://synphot.readthedocs.io/en/latest/', None)
}

# autodoc_default_flags = ['members', 'special-members',
#                          'inherited-members']
autodoc_member_order = 'bysource'

autosummary_generate = True

automodapi_toctreedirnm = 'api'
automodsumm_inherited_members = True


numpydoc_class_members_toctree = False
numpydoc_show_class_members = False
numpydoc_xref_param_type = True
numpydoc_attributes_as_param_list = True
numpydoc_xref_ignore = {'type', 'optional', 'default'}

# Class documentation should contain *both* the class docstring and
# the __init__ docstring
autoclass_content = "both"


ipython_savefig_dir = '_static/_generated'
ipython_warning_is_error = False
ipython_execlines = """\
from configobj import ConfigObj
import subprocess
import os, sys
import math
import numpy as np

module_path = os.path.abspath(os.path.join('../'))
if module_path not in sys.path:
    sys.path.append(module_path)
from moons1d.spectra import SimSpectrum, Template, Spectra , Sky, Atm_abs
from moons1d.simulator import Simulation
from moons1d.interface import run_v2, run_allbands

from astropy import units as u
from astropy.constants import c
from astropy.units import Quantity
from math import sqrt, radians, sin, cos, tan, pi, exp

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from synphot import SourceSpectrum , SpectralElement, units
from synphot.models import ConstFlux1D, Empirical1D
""".splitlines()


gitlab_url = 'https://gitlab.obspm.fr/mrodrigues/moons1d#'


pygments_style = 'sphinx'

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'classic'
on_rtd = os.environ.get('READTHEDOCS') == 'True'
if on_rtd:
    html_theme = 'default'
else:
    html_theme = 'nature'

html_show_sourcelink = False
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
