.. _configureSim:
*******************************
Configure simulation parameters 
*******************************

The parameters of the simulation are configured in two files: 
	* 'ObsTemplate.ini' - User congiguration file that mimics an ESO OB template. 
	* 'MOONS_mode.ini' - Static configuration file containing the instrumental properties for all observing MOONS modes. Not aim to be changed by the user. 

The following block of lines shows the content of the default ObsTemplate.ini to be change by the user to configure his/her simulation run. 

.. literalinclude:: SingleSource_config.ini
  :language: ini

In an ipython console or script file, start by loading the moons1D modules and configobj, and provide paths for the instrument configuration file (default is '../models/Instrument/'), the science templates and the output files

.. code-block:: python

   from moons1d.spectra import SimSpectrum, Template, Spectra , Sky, Atm_abs, flux2mag
   from moons1d.simulator import Simulation
   from configobj import ConfigObj

   Path_Inst_config = '../models/Instrument/'
   Path_template = '../models/Science/'
   Path_output = '../data/processed/'

Load the configuration file for the observation, here named 'SingleSource_config.ini', and the static instrument configuration file ('MOONS_mode.ini'). Initialize an instance of a  ``moons1d.simulator.Simulation`` object from the two configuration files. 

.. code-block:: python

   # Load Configuration files for observations and instrument parameters
   Obs_conf= ConfigObj('SingleSource_config.ini')
   MOONS_modes= ConfigObj(Path_Inst_config + 'MOONS_mode.ini')
   
   #Initiate the simulation object with the observation and instrument configuration
   Simu = Simulation(MOONS_modes,Obs_conf) 
   
A ``Simulation`` object contains all the information required to define a simulation run, and stores the intermediate and final 1D spectra associated to it. At initialisation, attributes storing configuration parameters are automatically set up using the ``SingleSource_config.ini`` file. The code automatically loads the instrument parameter for the mode/band specified in ``SingleSource_config.ini``. An empty  ``SimSpectrum``-type object, aim to store the simulated spectrum, is also initialized. 

