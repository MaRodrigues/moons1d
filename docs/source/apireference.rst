*************
Reference/API
*************


moons1d modules
===============

moons1d.spectra module
----------------------

.. automodule:: moons1d.spectra
   :members:
   :undoc-members:
   :show-inheritance:

moons1d.simulator module
------------------------

.. automodule:: moons1d.simulator
   :members:
   :undoc-members:
   :show-inheritance:

moons1d.interface module
------------------------

.. automodule:: moons1d.interface
   :members:
   :undoc-members:
   :show-inheritance:
