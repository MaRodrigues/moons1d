.. Moons1D documentation master file, created by
   sphinx-quickstart on Wed Jun 22 17:19:56 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Moons1D's documentation!
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. ifconfig:: 'dev' in release

    .. warning::
        This documentation is for the version of Moons1D currently under
        development.


Contents
========

.. toctree::
   :maxdepth: 1

   moons_simu
   apireference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
