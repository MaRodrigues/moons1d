*********************************************************************************
1D spectra simulator for MOONS (``moons1d.simulator``)
*********************************************************************************

The ``moons1d`` package provides a way to simulate reduced 1D data from MOONS.

The package consists on two modules ``moons1d.simulator``, containing the main functions
to simulate MOONS data, and ``moons1d.spectra`` storing utility classes and functions to handle
spectra.


Contents:

.. toctree::
   :maxdepth: 2

   configureSim
   SingleTargets
   MultipleTargets


Reference/API
=============
   apireference
